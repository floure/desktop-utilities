#!/bin/bash

BIN_TARGET="$HOME/.local/bin"

bold=$(tput bold)
normal=$(tput sgr0)

prompt_confirm() {
  while true; do
    read -r -n 1 -p "${1} [y/n]: " REPLY
    case $REPLY in
      [yY]) echo ; return 0 ;;
      [nN]) echo ; return 1 ;;
      *)printf " \033[31m %s \n\033[0m" "invalid input"
    esac
  done
}

echo "----------------------"
echo "   _   _ _   _ _      "
echo "  | | | | |_(_) |___  "
echo "  | | | | __| | / __| "
echo "  | |_| | |_| | \__ \ "
echo "   \___/ \__|_|_|___/ "
echo "                      "
echo "----------------------"

if ! [ -x "$(command -v stow)" ]; then
  echo 'Error: stow is not installed.' >&2
  exit 1
fi

echo ""
echo "${bold}Target for 'bin':${normal} ${BIN_TARGET}"

for tuple in bin,$BIN_TARGET etc,$XDG_CONFIG_HOME
do
  IFS=',' read src_path target_path <<< "${tuple}"
  echo "${bold}> Symbolic links to be created for '${src_path}':${normal}"
  stow --verbose 1 --simulate --target $target_path $src_path
  if prompt_confirm "${bold}Do yo want to proceed ?${normal}"; then
    stow --verbose 1 --target $target_path $src_path
  fi
  echo ""
done

echo "${bold}> Everything was processed. Bye!${normal}"
