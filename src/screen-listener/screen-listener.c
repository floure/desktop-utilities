#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>

#define CMD_BASE "screen-handler.sh"
#define CMD_BUFFER_SIZE 255
#define CMD_ARG_BUFF 255

Display *dpy = NULL;
int screen;

static void print_usage(void)
{
    printf("Usage: screen-listener\n");
}

static int spawn_handler(char* output, char* change)
{
    char cmd[CMD_BUFFER_SIZE];
    memset(cmd, '\0', CMD_BUFFER_SIZE);
    sprintf(cmd, "%s --output %s --action %s", CMD_BASE, output, change);

    printf("  -> Spawning handler: %s\n", cmd);
    int status = system(cmd);

    return status;
}

int main (int argc, char **argv)
{
    char *displayname = NULL;
    Window w = 0;
    int done;

    if (argc > 1) {
        print_usage();
        exit(EXIT_FAILURE);
    }

    dpy = XOpenDisplay (displayname);
    screen = DefaultScreen (dpy);
    w = RootWindow(dpy, screen);

    int mask = RROutputChangeNotifyMask;
    XRRSelectInput (dpy, w, mask);

    for (done = 0; !done;) {
        XEvent event;
        XRRScreenResources *screen_resources;
        XRROutputChangeNotifyEvent *e = NULL;
        XRROutputInfo *output_info = NULL;
        XRRModeInfo *mode_info = NULL;

        XNextEvent (dpy, &event);
        XRRUpdateConfiguration (&event);
        screen_resources = XRRGetScreenResources (dpy, ((XRRNotifyEvent*) &event)->window);
        e = (XRROutputChangeNotifyEvent *) &event;

        if (screen_resources) {
            output_info = XRRGetOutputInfo (dpy, screen_resources, e->output);
            for (int i = 0; i < screen_resources->nmode; i++)
                if (screen_resources->modes[i].id == e->mode) {
                    mode_info = &screen_resources->modes[i];
                    break;
                }
        }

        char output_name[CMD_ARG_BUFF+1];
        memset(output_name, '\0', CMD_ARG_BUFF+1);
        if (output_info) {
            strncpy(output_name, output_info->name, CMD_ARG_BUFF);
        } else {
            sprintf(output_name, "%lu", e->output);
        }

        char connection[CMD_ARG_BUFF+1];
        memset(connection, '\0', CMD_ARG_BUFF+1);
        switch (e->connection) {
        case RR_Connected:
            strncpy(connection, "connect", CMD_ARG_BUFF);
            break;
        case RR_Disconnected:
            strncpy(connection, "disconnect", CMD_ARG_BUFF);
            break;
        default:
            sprintf(connection, "%d", e->connection);
        }

        printf("New event: %s on %s\n", connection, output_name);

        spawn_handler(output_name, connection);
        XRRFreeOutputInfo (output_info);
    }

    XCloseDisplay (dpy);
    return EXIT_SUCCESS;
}
