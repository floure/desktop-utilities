Name:		epfl-scripts-git
Version:	64539c9366f993d3cce4fd3a452287c6336ad69d
Release:	1%{?dist}
Summary:	Collection of useful scripts when you are spending time at EPFL

License:	ASL 2.0
URL:		https://gitlab.gnugen.ch/gnugen/epfl-scripts
Source0:	https://gitlab.gnugen.ch/gnugen/epfl-scripts/repository/%{version}/archive.tar.gz

BuildArch: noarch
Requires:	coreutils
Requires:	curl
Requires:	file
Requires:	openconnect
Requires:	vpnc
Requires:	bash
Requires:	perl-File-MimeInfo
Requires:	perl-HTML-Tree
Requires:	perl-HTML-TreeBuilder-XPath
Requires:	perl-IO-stringy
Requires:	perl-Term-ANSIColor
Requires:	perl-WWW-Mechanize
Recommends:	(iptables and iproute)

%description
Collection of useful scripts when you are spending time at EPFL.


%prep
%autosetup -n epfl-scripts-%{version}-%{version}

%install
mkdir -p %{buildroot}/%{_bindir}
install -Dm 755 bin/epfl-vpn %{buildroot}/%{_bindir}/epfl-vpn
install -Dm 755 bin/gnupaste %{buildroot}/%{_bindir}/gnupaste
install -Dm 755 bin/pastegnugen.pl %{buildroot}/%{_bindir}/pastegnugen.pl
install -Dm 755 bin/tl.pl %{buildroot}/%{_bindir}/tl.pl
install -Dm 755 bin/velo.pl %{buildroot}/%{_bindir}/velo.pl

%files
%license LICENSE
%doc README.md
%{_bindir}/epfl-vpn
%{_bindir}/gnupaste
%{_bindir}/pastegnugen.pl
%{_bindir}/tl.pl
%{_bindir}/velo.pl

%changelog
* Wed Nov 8 2017 Timothée Floure <timothee.floure@fnux.ch> - 64539c9366f993d3cce4fd3a452287c6336ad69d-1
- Let there be package.
