%global debug_package %{nil}
%define commit f52c9614b51ac2190c98988f64f6f68fd194d9f7

Name:     pavolume-git
Version:  0
Release:  2.%{commit}%{?dist}
Summary:  CLI tool to change the volume of the default audio sink of PulseAudi

License:  GPLv3
URL:      https://github.com/florv/pavolume
Source0:  %{url}/archive/f52c9614b51ac2190c98988f64f6f68fd194d9f7.zip

BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  pulseaudio-libs-devel

%description
Small command-line tool allowing you to change the volume of the default audio
sink of the PulseAudio server.

%prep
%autosetup -n pavolume-%{commit}

%build
make

%install
mkdir -p %{buildroot}%{_bindir}/
cp -p pavolume %{buildroot}%{_bindir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/pavolume

%changelog
* Fri May 10 2019 Timothée Floure <fnux@fedoraproject.org> - 0-2.f52c9614b51ac2190c98988f64f6f68fd194d9f7
- Update package release schema for proper ordering.

* Mon Oct 30 2017 Timothée Floure <fnux@fedoraproject.org> - f52c9614b51ac2190c98988f64f6f68fd194d9f7-1
- Let there be package.
