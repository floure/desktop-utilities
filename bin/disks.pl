#!/usr/bin/perl -w

use strict;
use Curses::UI;
use JSON;
use Data::Dumper;

my $lsblk = "/usr/bin/lsblk";
my $udisksctl = "/usr/bin/udisksctl";
my %devices;
my $cui = new Curses::UI( -color_support => 1);

# ----------------------------------------------------------------------
# Methods
# ----------------------------------------------------------------------

sub get_lsblk {
  my $json = `$lsblk --json --output name,size,type,mountpoint,fstype,label`;
  JSON->new->utf8->decode($json);
}
sub build_lists {
  my ($values, $labels) = @_;
  @$values = ();
  %$labels = ();
  my $json = get_lsblk();
  foreach my $blockdevice (@{%$json{'blockdevices'}}) {
    push @$values, $$blockdevice{'name'};
    my $label = "/dev/" . $$blockdevice{'name'} . " - " . $$blockdevice{'size'}
    . " - " . $$blockdevice{'type'};
    $$labels{$$blockdevice{'name'}} = $label;
    push_children($values, $labels, $blockdevice, "    ");
  }
}

sub push_children;
sub push_children{
  my ($values, $labels, $source, $prefix) = @_;
  my $children = $source->{'children'};

  if ($children) {
    foreach my $child (@{$children}) {
      # list value
      my $value = "/dev$prefix/$$child{'name'}";
      $value =~ tr/ //ds; # remove spaces

      # devices hash
      $devices{$value} = $$child{'mountpoint'};

      # list label
      push @$values, $value;
      my $label = "$prefix/$$child{'name'} - $$child{'size'} - "
      . "$$child{'type'} $$child{'fstype'}";
      if ($$child{'mountpoint'}) {
        $label = "$label : <underline>$$child{'mountpoint'}</underline>"
      }
      my $prefix = $prefix . "/" . $$child{'name'};
      $$labels{$value} = $label;

      push_children($values, $labels, $child, $prefix);
    }
  }
}

# `2>&1` at the end sends standard error to the same place as standard output,
# which is captured by the back-quotes.
sub mount {
  my $device = shift;
  `$udisksctl mount --block-device $device 2>&1`
}
sub unmount {
  my $device = shift;
 `$udisksctl unmount --block-device $device 2>&1`
}

# ----------------------------------------------------------------------
# Create the explanation window
# ----------------------------------------------------------------------
 
my $w0 = $cui->add(
    'w0', 'Window',
    -border        => 1,
    -y             => -1,
    -height        => 3,
);
$w0->add('explain', 'Label',
  -text => "q: quit   r: refresh    h/j/k/l or arrows: move   enter: select"
);

# ----------------------------------------------------------------------
# Create the main window
# ----------------------------------------------------------------------

my %args = (
    -border       => 1,
    -titlereverse => 0,
    -padtop       => 0,
    -padbottom    => 3,
    -ipad         => 1,
);

my $w = $cui->add(
  "main", 'Window',
  -title => "Disks.pl",
  %args
);

# ----------------------------------------------------------------------
# Fill main window
# ----------------------------------------------------------------------
$w->add(
    undef, 'Label',
    -text => "This script allows you to mount/umount"
    . " partitions without sudo/root. \nIt's a simple interface over"
    . " `udisksctl`.\n"
);
$w->add(undef,'Label',-text=>"Available partitions",-y=>4,-bold=>1 );

# Print fancy list :3
my @values;
my %labels;
build_lists(\@values, \%labels);
my $listbox = $w->add(
    'mylistbox', 'Listbox',
    -values => \@values,
    -labels => \%labels,
    -htmltext => 1,
    -y => 6
);

# ----------------------------------------------------------------------
# Setup binding, focus and actions
# ----------------------------------------------------------------------

$cui->set_binding( sub{ exit }, "q" );
$cui->set_binding(
  sub{ build_lists(\@values, \%labels); $listbox->draw(); },
  "r"
);

# Set focus on the main list
$listbox->focus();

# Set action on select
$listbox->onChange(
  sub {
    my $value = $listbox->get();
    my $mountpoint = $devices{$value};

    my $msg;
    unless ($mountpoint) { $msg = "Mount $value ?"; }
    else { $msg = "Unmount $value (mounted at $mountpoint) ?"; }

    my $choice = $cui->dialog(
      -message => $msg,
      -buttons => ['ok', 'cancel'],
      -title => $value,
    );

    if ($choice) {
      my $output;
      unless ($mountpoint) { $output = mount($value); } else { $output = unmount($value); }
      $cui->root->dialog(
        -message => "Output : $output",
        -title   => "Command output"
      );
      build_lists(\@values, \%labels);
      $listbox->draw();
    }

    # Stay here !
    $listbox->focus();
    $listbox->clear_selection();
  }
);

# ----------------------------------------------------------------------
# Get things rolling...
# ----------------------------------------------------------------------

$cui->mainloop;
