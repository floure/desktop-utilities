#!/bin/sh

dest=$HOME/Pictures/Screenshots

cd $HOME
files=$(find . -maxdepth 1 -iname "*.png")

if [[ $files == "" ]]; then
  echo "No screenshot found in $HOME."
  exit;
fi

for file in $files; do
  echo -n "Moving $(basename $file) into $dest... "
  mv $file $dest
  echo "ok"
done
