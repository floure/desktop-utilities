#!/bin/sh
set -e

MNT="/mnt/backup-volume"
RSYNC="rsync -aHAXxh --delete --sparse --info=progress2"
PROGNAME="$(basename "$0")"
DIRECTORIES="/ /home /var"

DISK="/dev/disk/by-uuid/6456e7cf-c3a0-439d-818f-52aa8462af02"
PASS_NAME="misc/wd-passport"
CRYPTSETUP_NAME="wd-mypassport-crypt"
VG_NAME="wd-mypassport"
FS_DEVICE="/dev/$VG_NAME/backup"

help ()
{
	cat <<-EOF
	Usage:
		$PROGNAME [backup|mount|umount]
	EOF
}

say ()
{
	printf "$PROGNAME: %s\n" "$*"
}

die ()
{
	say "$*"; exit 1
}

mountfs ()
{
	mkdir -p "$MNT"
	echo -n "Opening luks encrypted volume $CRYPTSETUP_NAME... "
	sudo -u "$USERNAME" PASSWORD_STORE_DIR="$PASSWORD_STORE_DIR" \
		GNUPGHOME="$GNUPGHOME" pass show "$PASS_NAME" \
		| cryptsetup open --type luks "$DISK" "$CRYPTSETUP_NAME"
	udevadm settle
	sleep 1
	echo "Done"

	echo -n "Activating LVM volume group $VG_NAME... "
	vgs -o vg_name --noheadings \
		| grep "$VG_NAME" >/dev/null 2>&1 || die "Can't find ‘$VG_NAME’ vg."
	vgchange -ay "$VG_NAME";

	echo -n "Mounting $FS_DEVICE into $MNT... "
	mount "$FS_DEVICE" "$MNT"
	echo "Done"

	tput setaf 2; echo "The backup volume was successfuly mounted."; tput sgr0
}

umountfs ()
{
	echo -n "Unmounting $MNT... "
	umount "$MNT"
	rmdir "$MNT"
	echo "Done"

	echo -n "Disable LVM volume group $VG_NAME... "
	vgchange -an "$VG_NAME"
	echo -n "Closing luks colume $CRYPTSETUP_NAME... "
	cryptsetup close "$CRYPTSETUP_NAME"
	sync
	echo "Done"
	tput setaf 2; echo "You may now safely disconnect the drive."; tput sgr0
}

backup ()
{
	mountfs

	for directory in $DIRECTORIES; do
		echo "Copying $directory..."
		$RSYNC $directory "$MNT/current$directory" || true
	done

	echo -n "Creating BTRFS snapshot... "
	mkdir -p "$MNT/snapshots"
	btrfs subvolume snapshot -r "$MNT/current" \
		"$MNT/snapshots/$(date --rfc-3339=seconds)"

	tput setaf 3
	df -h | grep "^Filesystem\|$MNT"
	tput sgr0

	umountfs
}

if [ ! "$(hostname)" = "phi" ]
then
	die "This script must be run on phi."
fi

if [ ! -e "$DISK" ]
then
	die "Couldn't find backup disk."
fi

if [ ! "$USER" = "root" ]
then
	exec sudo USERNAME="$(whoami)" PASSWORD_STORE_DIR="$PASSWORD_STORE_DIR" \
		GNUPGHOME="$GNUPGHOME" "$0" "$@"
fi

case "$1" in
	backup) backup ;;
	mount)  mountfs ;;
	umount) umountfs ;;
	*)      help ;;
esac
