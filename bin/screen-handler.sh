#!/bin/bash

if [ "$#" -lt 4 ]; then
  echo "Missing arguments !"
  exit 1
fi

while [ "$#" -gt 0 ]; do
  case "$1" in
    --output) output="$2"; shift 2;;
    --action) action="$2"; shift 2;;
    *) echo "Unknown parameter passed: $1"; exit 1;;
  esac
done

echo "Marching: $action $output"
case "$output $action" in
  "DP-1 connect")
    xrandr --output DP-1 --mode 2560x1080 --pos 1920x0 --rotate normal
    nitrogen --restore
    ;;
  "DP-1 disconnect")
    xrandr --output DP-1 --off
    nitrogen --restore
    ;;
  *)
    echo "-> Unknown combination."
    ;;
esac
