#!/usr/bin/env ruby

require 'yaml'
require 'icinga2'

class Output
  def initialize(width = 40)
    @column = 0
    @line = 0
    @line_witdh = width
    @content = Array.new()
  end

  def append(text)
    current = @content.at(@line)
    if current.nil?
      new = " " * @line_witdh * @column
    else
      new = current.ljust(@line_witdh * @column)
    end

    new += text
    @content[@line] = new
    @line += 1
  end

  def switch_to_next_column
    @column += 1
    @line = 0
  end

  def print
    @content.each { |l| puts l }
  end
end

def config_path
  home = ENV.fetch('HOME')
  ENV.fetch('XDG_CONFIG_HOME', home + "/.config") + "/icinga2-status.yaml"
end

def get_password(path)
  if path.nil?
    nil
  else
    raw = `/usr/bin/pass #{path}`
    raw.chomp
  end
end

def get_config
  raw = File.read(config_path)
  hash = YAML.load(raw)

  instances = Array.new()

  hash.keys.each do |k|
    c = {
      icinga: {
        host: k,
        api: {
          port: hash[k].fetch("port", 5665),
          username: hash[k].fetch("user", "admin"),
          password: get_password(hash[k].fetch("pass", nil)),
          pki_path: hash[k].fetch("pki_path", "/etc/icinga2"),
          node_name: hash[k].fetch("node_name", nil)
        }
      }
    }

    instances.push(c)
  end

  instances
end

def build_overview(i)
  labels = ["all", "warning", "critical", "unknown", "pending", "in_downtime",
            "acknowledged", "adj. warning", "adj. critical", "adj. unknown", "handled
  all", "handled_warning", "handled_warning", "handled_critical",
  "handled_unknown"]
  values = i.service_problems.values
  labels.zip(values).to_h
end

def build_problem_list(i)
  i.list_services_with_problems.values[0]
end

def fill_with(i, output)
  overview = build_overview(i)
  problems = build_problem_list(i)

  output.append("=> Instance #{i.node_name}")
  output.append("")
  output.append("==== Overview ====")
  output.append(format("   - All:      %d", overview["all"]))
  output.append(format("   - Warning:  %d", overview["warning"]))
  output.append(format("   - Critical: %d", overview["critical"]))
  output.append("")
  output.append("==== Problems ====")
  output.append("")
  problems.each { |p| output.append(format("   - %s", p))}
  output.append("")

  output.switch_to_next_column()
end

def main
  configs = get_config()
  output = Output.new()

  configs.each do |c|
    print "Fetching from #{c[:icinga][:host]}... "
    i = Icinga2::Client.new(c)

    if i.nil?
      puts "Failed to initialize client."
      exit(1)
    end

    fill_with(i, output)
    puts "OK"
  end

  puts ""
  output.print()
end

main()
