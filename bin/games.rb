#!/usr/bin/ruby

BASE_DIRECTORY = File.join(ENV['HOME'], ".local/opt/games")
ENTRY_STRING_LENGTH = 30

class Game
  attr_reader :name, :version, :directory, :executable

  def self.from_directory(dir)
    name = dir
    version = nil
    game_directory = File.join(BASE_DIRECTORY, dir)
    executable = nil

    # Extract name and version from GOG's gameinfo
    gameinfo_path = File.join(game_directory, "gameinfo")
    if File.exists?(gameinfo_path)
      lines = IO.readlines(gameinfo_path, chomp: true)
      name = lines[0]
      version = lines[1]
    end

    # Prefer the provided launcher
    for filename in ["start.sh", "run.sh"] do
      if executable.nil?
        path = File.join(game_directory, filename)
        if File.exists?(path) && File.executable?(path)
          executable = path
        end
      end
    end

    # Default to the first executable found in the game's root directory
    if executable.nil?
      files = Dir.children(game_directory).select{
        |f| !File.directory?(f) && File.executable?(f)
      }
      if !files.empty?
        executable = files.first
      end
    end

    self.new(name, version, game_directory, executable)
  end

  def initialize(name, version, directory, executable)
    @name = name
    @version = version
    @directory = directory
    @executable = executable
  end
end

def scan(base_directory)
  directories = Dir.children(base_directory).select do |f|
    File.directory?(File.join(base_directory, f))
  end

  games = Array.new()

  directories.each do |dir|
    game = Game.from_directory(dir)
    games.push(game)
  end

  return games
end

def select(games)
  puts "- - - - - - - - - - - - - - - - - - - - - - -"
  puts "Simple game launcher // Please select a game:"
  puts "- - - - - - - - - - - - - - - - - - - - - - -"
  print "\n"

  games.each_index do |i|
    game = games[i]
    entry = "#{i}) #{game.name}".slice(0..(ENTRY_STRING_LENGTH-1))
    entry += (" " * [0, ENTRY_STRING_LENGTH - entry.length].max)

    if (i % 2 == 1) then puts entry else print entry end
  end

  print "\n"
  selected = nil
  while (selected.nil?) do 
    print "Please select one of the above entries: "
    raw = gets.chomp

    if raw == "q" then puts "Exiting."; exit end
    selected = raw.to_i if (0..(games.size-1)).include?(raw.to_i)
  end

  print "\n"

  return games[selected]
end

def run(game)
  version = if game.version == nil then "?.?.?" else game.version end
  executable = if game.executable == nil then "/bin/false" else game.executable.gsub(/ /, '\ ') end

  puts "- - - - - - - - - - - - - - - - - - - - - - -"
  puts "=> selected #{game.name} - v#{version}."
  puts "- - - - - - - - - - - - - - - - - - - - - - -"

  puts "Executable: #{executable}"

  run = nil
  print "Run? [y/n/q] "
  while run.nil? do
    raw = gets.chomp
    case raw
    when "q"
      abort("Exiting")
    when "y"
      run = true
      log_file = File.join(game.directory, Time.now.to_s.gsub(/ /, '') + ".log").gsub(/ /, '\ ')
      puts "/!\\ logs will be saved into #{log_file}"
      command = "/usr/bin/xterm -e \"#{executable} | /usr/bin/tee -a #{log_file}\""
      puts "Executing: #{command}"
      exit_value = system(command)
      puts "Exited: #{exit_value}. Bye!"
    when "n"
      run = false
      main()
    end
  end
end

def main()
  abort(BASE_DIRECTORY + " does not exsist!") if !Dir.exists?(BASE_DIRECTORY)
  games = scan(BASE_DIRECTORY)
  game = select(games)
  run(game)  
end

main()
